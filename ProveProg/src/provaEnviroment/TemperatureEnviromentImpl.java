package provaEnviroment;

public class TemperatureEnviromentImpl extends AbstractEnviroment implements TemperatureEnviroment {

	private int temperature;
	private final BasicEnviroment basicEnviroment;
	
	
	
	public TemperatureEnviromentImpl(BasicEnviroment basicEnviroment, int temperature) {
		this.basicEnviroment = basicEnviroment;
		this.temperature = temperature;
	}



	@Override
	public int getTemperature() {
		return this.temperature;
	}



	@Override
	public int getInitialFoodQuantity() {
		return this.basicEnviroment.getInitialFoodQuantity();
	}

}
