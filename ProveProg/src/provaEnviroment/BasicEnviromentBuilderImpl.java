package provaEnviroment;

import java.util.Optional;

public class BasicEnviromentBuilderImpl implements EnviromentBuilder {
	
	private Optional<Integer> initialFoodQuantity;
	
	
	
	public BasicEnviromentBuilderImpl() {
		this.initialFoodQuantity = Optional.empty();
	}



	@Override
	public EnviromentBuilder addInitialFoodQuantity(int initialFoodQuantity) {
		this.initialFoodQuantity = Optional.of(initialFoodQuantity);
		return this;
	}

	@Override
	public BasicEnviroment build() {
		if (this.initialFoodQuantity.isEmpty()) {
			throw (new IllegalStateException());
		}
		return new BasicEnviromenImpl(this.initialFoodQuantity.get());
	}

}
