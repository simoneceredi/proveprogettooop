package provaEnviroment;

public class BasicEnviromenImpl extends AbstractEnviroment implements BasicEnviroment {

	private final int initialFoodQuantity;
	
	public BasicEnviromenImpl(int initialFoodQuantity) {
		this.initialFoodQuantity = initialFoodQuantity;
	}

	@Override
	public int getInitialFoodQuantity() {
		return this.initialFoodQuantity;
	}
	
}
