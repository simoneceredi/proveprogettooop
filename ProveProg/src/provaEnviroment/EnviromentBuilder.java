package provaEnviroment;

public interface EnviromentBuilder {
	EnviromentBuilder addInitialFoodQuantity(int initialFoodQuantity);
	BasicEnviroment build();
}
