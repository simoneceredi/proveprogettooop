package provaEnviroment;

public class TemperatureEnviromentBuilderImpl implements TemperatureEnviromentBuilder {

	private int temp;
	private final EnviromentBuilder eb;
	
	public TemperatureEnviromentBuilderImpl(EnviromentBuilder eb) {
		this.eb = eb;
	}

	@Override
	public TemperatureEnviromentBuilder addTemperature(int temp) {
		this.temp = temp;
		return this;
	}

	@Override
	public TemperatureEnviroment build() throws IllegalStateException {
		return new TemperatureEnviromentImpl(this.eb.build(), this.temp);
	}

}
