package provaEnviroment;

public interface TemperatureEnviromentBuilder {
	TemperatureEnviromentBuilder addTemperature(int temp);
	TemperatureEnviroment build();
}
