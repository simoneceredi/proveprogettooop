package cosine;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Cosine {

	public static void main(String[] args) {
		Map<Integer, Set<String>> m = new HashMap<>();
		Set<String> si = new HashSet<>();
		si.add("si");
		si.add("forse");
		Set<String> no = new HashSet<>();
		no.add("no");
		m.put(1, si);
		m.put(2, no);
		System.out.println(m);
		m.values().stream().filter(s -> s.contains("si"))
			.collect(Collectors.toList())
			.get(0)
			.remove("a");
		System.out.println(m);
		add("si", "no", "forse");
		Set<String> org = new HashSet<>();
        m.values().forEach(org::addAll);
        Iterator<String> i = org.iterator();
        while(i.hasNext()) {
        	System.out.println(i.next());
        }
	}
	private static void add(String...e) throws IllegalStateException {
		Set<String> s = new HashSet<>();
		s.add("ciao");
		System.out.println(s);
		Collections.addAll(s, e);
		System.out.println(s);
		throw(new IllegalStateException());
	}
}
